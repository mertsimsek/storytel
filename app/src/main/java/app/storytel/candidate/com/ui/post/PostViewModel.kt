package app.storytel.candidate.com.ui.post

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.storytel.candidate.com.data.post.remote.PostRemoteDataSource
import app.storytel.candidate.com.data.post.remote.PostService
import app.storytel.candidate.com.di.ServiceLocator
import app.storytel.candidate.com.repository.post.PostRepository
import app.storytel.candidate.com.util.extensions.disposeIfNeed
import app.storytel.candidate.com.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PostViewModel(private val postRepository: PostRepository) : ViewModel() {

    private val postActivityViewStateLiveData = MutableLiveData<PostActivityViewState>()

    private val compositeDisposable = CompositeDisposable()

    init {
        compositeDisposable += postRepository.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { postActivityViewStateLiveData.value = PostActivityViewState(it) }
    }

    fun getActivityViewStateLiveData() = postActivityViewStateLiveData

    fun refresh() {
        compositeDisposable += postRepository.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { postActivityViewStateLiveData.value = PostActivityViewState(it) }
    }

    override fun onCleared() {
        compositeDisposable.disposeIfNeed()
        super.onCleared()
    }

}