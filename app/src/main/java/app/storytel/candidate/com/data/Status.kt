package app.storytel.candidate.com.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING;

    fun isSuccess(): Boolean {
        return SUCCESS == this
    }

    fun isError(): Boolean {
        return ERROR == this
    }

    fun isLoading(): Boolean {
        return LOADING == this
    }

    fun isNotLoading(): Boolean {
        return LOADING != this
    }
}
