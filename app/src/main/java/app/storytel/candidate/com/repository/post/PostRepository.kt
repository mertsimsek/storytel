package app.storytel.candidate.com.repository.post

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.post.remote.PostRemoteDataSource
import app.storytel.candidate.com.data.post.remote.model.PostAndPhotoModel
import io.reactivex.Observable

class PostRepository(private val postRemoteDataSource: PostRemoteDataSource) {

    fun getPosts(): Observable<Resource<List<PostAndPhotoModel>>> {
        return postRemoteDataSource.getPosts()
    }
}