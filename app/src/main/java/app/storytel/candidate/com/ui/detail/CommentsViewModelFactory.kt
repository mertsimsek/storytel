package app.storytel.candidate.com.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.storytel.candidate.com.repository.comment.CommentRepository
import java.lang.IllegalStateException

class CommentsViewModelFactory(private val commentRepository: CommentRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == CommentsViewModel::class.java) {
            return CommentsViewModel(commentRepository) as T
        } else {
            throw IllegalStateException("Can no create instance of ViewModel")
        }
    }
}