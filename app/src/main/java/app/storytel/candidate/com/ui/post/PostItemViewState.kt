package app.storytel.candidate.com.ui.post

import app.storytel.candidate.com.data.post.remote.model.PostAndPhotoModel

data class PostItemViewState(val postAndPhotoModel: PostAndPhotoModel) {

    fun getPostId(): String {
        return postAndPhotoModel.postModel.id.toString()
    }

    fun getPostPhotoUrl(): String {
        return postAndPhotoModel.photoModel.thumbnailUrl ?: ""
    }

    fun getPostTitle(): String {
        return postAndPhotoModel.postModel.title?.capitalize() ?: ""
    }

    fun getPostBody(): String {
        return postAndPhotoModel.postModel.title?.capitalize() ?: ""
    }
}