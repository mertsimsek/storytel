package app.storytel.candidate.com.ui.post

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.post.remote.model.PostAndPhotoModel

data class PostActivityViewState(val postResponseResource: Resource<List<PostAndPhotoModel>>) {

    fun isLoading(): Boolean {
        return postResponseResource.isLoading()
    }

    fun hasError(): Boolean {
        return postResponseResource.isError()
    }

    fun getPostItemViewStateList(): List<PostItemViewState> {
        val postItemViewStateList = arrayListOf<PostItemViewState>()
        postResponseResource.data?.forEach { postItemViewStateList.add(PostItemViewState(it)) }
        return postItemViewStateList
    }
}