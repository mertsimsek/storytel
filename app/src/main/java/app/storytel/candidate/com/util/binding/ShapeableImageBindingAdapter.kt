package app.storytel.candidate.com.util.binding

import androidx.databinding.BindingAdapter
import com.google.android.material.imageview.ShapeableImageView

@BindingAdapter("cornerRadius")
fun cornerRadius(shapeableImageView: ShapeableImageView, cornerRadius: Float) {
    shapeableImageView.shapeAppearanceModel = shapeableImageView.shapeAppearanceModel
        .toBuilder()
        .setAllCornerSizes(cornerRadius)
        .build()
}