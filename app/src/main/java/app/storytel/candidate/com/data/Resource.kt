package app.storytel.candidate.com.data

class Resource<T> private constructor(
        val status: Status,
        val data: T?,
        val error: Throwable? = null
) {

    fun isSuccess() = status == Status.SUCCESS

    fun isLoading() = status == Status.LOADING

    fun isError() = status == Status.ERROR

    companion object {
        fun <T> error(data: T?, error: Throwable) =
            Resource(
                Status.ERROR,
                data,
                error
            )

        fun <T> success(data: T) = Resource(
            Status.SUCCESS,
            data
        )

        fun <T> loading(data: T?) = Resource(
            Status.LOADING,
            data
        )
    }
}