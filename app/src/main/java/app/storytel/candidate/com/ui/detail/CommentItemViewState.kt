package app.storytel.candidate.com.ui.detail

import app.storytel.candidate.com.data.comment.remote.model.CommentModel

data class CommentItemViewState(private val comment: CommentModel) {

    fun getCommentTitle(): String {
        return comment.name?.capitalize() ?: "Unknown"
    }

    fun getCommentBody(): String {
        return comment.body?.capitalize() ?: "No comment."
    }
}