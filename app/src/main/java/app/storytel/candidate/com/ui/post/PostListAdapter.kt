package app.storytel.candidate.com.ui.post

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.ItemPostBinding
import app.storytel.candidate.com.util.extensions.inflateAdapterItem
import java.lang.IllegalStateException

class PostListAdapter : RecyclerView.Adapter<PostListAdapter.PostItemViewHolder>() {

    private val postItemViewStateList = arrayListOf<PostItemViewState>()

    var itemClickListener: ((PostItemViewState) -> Unit)? = null

    fun setPostItemViewStates(postItemViewStateList: List<PostItemViewState>) {
        this.postItemViewStateList.clear()
        this.postItemViewStateList.addAll(postItemViewStateList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostItemViewHolder = PostItemViewHolder.create(parent, itemClickListener)

    override fun getItemCount(): Int = postItemViewStateList.size

    override fun onBindViewHolder(holder: PostItemViewHolder, position: Int) = holder.bind(postItemViewStateList[position])

    class PostItemViewHolder(private val binding: ItemPostBinding,
                             private val itemClickListener: ((PostItemViewState) -> Unit)?) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.layoutPostRoot.setOnClickListener {
                itemClickListener?.invoke(binding.itemViewState
                        ?: throw IllegalStateException("ViewState is not set"))
            }
        }

        fun bind(postItemViewState: PostItemViewState) {
            binding.itemViewState = postItemViewState
            binding.executePendingBindings()
        }

        companion object {

            fun create(parent: ViewGroup, itemClickListener: ((PostItemViewState) -> Unit)?): PostItemViewHolder {
                val binding: ItemPostBinding = parent.inflateAdapterItem(R.layout.item_post)
                return PostItemViewHolder(binding, itemClickListener)
            }
        }
    }
}