package app.storytel.candidate.com.data.comment.remote

import app.storytel.candidate.com.data.comment.remote.model.CommentModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface CommentService {

    @GET("/posts/{id}/comments")
    fun getComments(@Path("id") postId: String): Single<List<CommentModel>>

}