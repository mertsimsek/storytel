package app.storytel.candidate.com.repository.comment

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.comment.remote.CommentRemoteDataSource
import app.storytel.candidate.com.data.comment.remote.model.CommentModel
import io.reactivex.Observable

class CommentRepository(private val commentRemoteDataSource: CommentRemoteDataSource) {

    fun getComments(postId: String): Observable<Resource<List<CommentModel>>> {
        return commentRemoteDataSource.getComments(postId)
    }

}