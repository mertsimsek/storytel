package app.storytel.candidate.com.util.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable?.disposeIfNeed() {
    if (this?.isDisposed?.not() == true) {
        this.dispose()
    }
}

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}