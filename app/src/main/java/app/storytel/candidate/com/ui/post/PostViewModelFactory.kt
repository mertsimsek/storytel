package app.storytel.candidate.com.ui.post

import androidx.lifecycle.ViewModel

import androidx.lifecycle.ViewModelProvider
import app.storytel.candidate.com.repository.post.PostRepository
import java.lang.IllegalStateException

class PostViewModelFactory(private val postRepository: PostRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == PostViewModel::class.java) {
            return PostViewModel(postRepository) as T
        } else {
            throw IllegalStateException("Can no create instance of ViewModel")
        }
    }
}