package app.storytel.candidate.com.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.ActivityCommentsBinding
import app.storytel.candidate.com.di.ServiceLocator
import app.storytel.candidate.com.ui.dialog.error.ErrorDialog
import app.storytel.candidate.com.util.delegate.contentView

class CommentsActivity : AppCompatActivity() {

    private val binding: ActivityCommentsBinding by contentView(R.layout.activity_comments)

    private val commentListAdapter = CommentListAdapter()

    private lateinit var commentsViewModel: CommentsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.recyclerViewComments.adapter = commentListAdapter

        binding.refreshLayout.setOnRefreshListener { commentsViewModel.refresh() }

        val commentsViewModelFactory = CommentsViewModelFactory(ServiceLocator.getCommentsRepository())
        commentsViewModel = ViewModelProvider(this, commentsViewModelFactory).get(CommentsViewModel::class.java)

        commentsViewModel.initialize(loadPostIdFromIntent())

        commentsViewModel.getCommentsActivityViewState()
                .observe(this, Observer { renderViewState(it) })
    }

    private fun renderViewState(commentsActivityViewState: CommentsActivityViewState) {
        commentListAdapter.setCommentItemViewStates(commentsActivityViewState.getCommentItemViewStateList())

        binding.viewState = commentsActivityViewState
        binding.executePendingBindings()

        if (commentsActivityViewState.hasError()) {
            showErrorDialog()
        }
    }

    private fun loadPostIdFromIntent(): String? {
        return intent?.extras?.getString(KEY_BUNDLE_POST_ID)
    }

    private fun showErrorDialog() {
        val errorDialog = ErrorDialog.newInstance()
                .apply { setOnActionClicked { commentsViewModel.refresh() } }
        errorDialog.show(supportFragmentManager, "")
    }

    companion object {

        private const val KEY_BUNDLE_POST_ID = "KEY_BUNDLE_POST_ID"

        fun newIntent(context: Context, postId: String): Intent {
            return Intent(context, CommentsActivity::class.java).apply {
                putExtra(KEY_BUNDLE_POST_ID, postId)
            }
        }
    }
}