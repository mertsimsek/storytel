package app.storytel.candidate.com.di

import app.storytel.candidate.com.data.comment.remote.CommentRemoteDataSource
import app.storytel.candidate.com.data.comment.remote.CommentService
import app.storytel.candidate.com.data.post.remote.PostRemoteDataSource
import app.storytel.candidate.com.data.post.remote.PostService
import app.storytel.candidate.com.repository.comment.CommentRepository
import app.storytel.candidate.com.repository.post.PostRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Note: Dagger or Koin can be used in here.
 * I try to keep the project simple.
 * Those libraries can easily be implemented if required.
 *
 */
object ServiceLocator {

    /**
     * Common instances
     */
    private val okHttpClient = OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()


    private val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    /**
     * Post singleton instances
     */
    private val postService = retrofit.create(PostService::class.java)

    private val postRemoteDataSource = PostRemoteDataSource(postService)

    private val postRepository = PostRepository(postRemoteDataSource)

    /**
     * Comment singleton instances
     */
    private val commentsService = retrofit.create(CommentService::class.java)

    private val commentsRemoteDataSource = CommentRemoteDataSource(commentsService)

    private val commentRepository = CommentRepository(commentsRemoteDataSource)

    fun getPostRepository(): PostRepository {
        return postRepository
    }

    fun getCommentsRepository(): CommentRepository {
        return commentRepository
    }
}