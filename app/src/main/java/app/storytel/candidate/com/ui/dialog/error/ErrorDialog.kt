package app.storytel.candidate.com.ui.dialog.error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.DialogErrorBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ErrorDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogErrorBinding

    private var onActionClicked: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BaseBottomDialogTheme)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_error, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonTryAgain.setOnClickListener {
            dismissAllowingStateLoss()
            onActionClicked?.invoke()
        }
    }

    fun setOnActionClicked(onActionClicked: () -> Unit) {
        this.onActionClicked = onActionClicked
    }

    companion object {

        fun newInstance(): ErrorDialog {
            return ErrorDialog()
        }
    }
}