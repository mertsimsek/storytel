package app.storytel.candidate.com.util.binding

import androidx.databinding.BindingAdapter
import com.google.android.material.imageview.ShapeableImageView
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun loadImageUrl(imageView: ShapeableImageView, imageUrl: String?) {
    if (imageUrl.isNullOrEmpty().not()) {
        Picasso.get()
                .load(imageUrl)
                .into(imageView)
    }
}