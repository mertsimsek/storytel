package app.storytel.candidate.com.ui.post

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.ActivityPostBinding
import app.storytel.candidate.com.di.ServiceLocator
import app.storytel.candidate.com.ui.detail.CommentsActivity
import app.storytel.candidate.com.ui.dialog.error.ErrorDialog
import app.storytel.candidate.com.util.delegate.contentView

class PostActivity : AppCompatActivity() {

    private val binding: ActivityPostBinding by contentView(R.layout.activity_post)

    private val postListAdapter = PostListAdapter()

    private lateinit var postViewModel: PostViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        postListAdapter.itemClickListener = { startCommentsActivity(it.getPostId()) }

        binding.refreshLayout.setOnRefreshListener { postViewModel.refresh() }

        binding.recyclerViewPost.adapter = postListAdapter

        val postViewModelFactory = PostViewModelFactory(ServiceLocator.getPostRepository())
        postViewModel = ViewModelProvider(this, postViewModelFactory).get(PostViewModel::class.java)
        postViewModel.getActivityViewStateLiveData()
                .observe(this, Observer { renderViewState(it) })
    }

    private fun renderViewState(postActivityViewState: PostActivityViewState) {
        postListAdapter.setPostItemViewStates(postActivityViewState.getPostItemViewStateList())

        binding.viewState = postActivityViewState
        binding.executePendingBindings()

        if (postActivityViewState.hasError()) {
            showErrorDialog()
        }
    }

    private fun startCommentsActivity(postId: String) {
        val commentsIntent = CommentsActivity.newIntent(this, postId)
        startActivity(commentsIntent)
    }

    private fun showErrorDialog() {
        val errorDialog = ErrorDialog.newInstance()
                .apply { setOnActionClicked { postViewModel.refresh() } }
        errorDialog.show(supportFragmentManager, "")
    }
}