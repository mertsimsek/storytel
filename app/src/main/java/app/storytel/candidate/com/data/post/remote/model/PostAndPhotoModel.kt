package app.storytel.candidate.com.data.post.remote.model

data class PostAndPhotoModel(val postModel: PostModel, val photoModel: PhotoModel)