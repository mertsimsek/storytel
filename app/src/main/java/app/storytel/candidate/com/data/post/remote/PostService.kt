package app.storytel.candidate.com.data.post.remote

import app.storytel.candidate.com.data.post.remote.model.PhotoModel
import app.storytel.candidate.com.data.post.remote.model.PostModel
import io.reactivex.Single
import retrofit2.http.GET

interface PostService {

    @GET("/posts")
    fun getPosts(): Single<List<PostModel>>

    @GET("/photos")
    fun getPhotos(): Single<List<PhotoModel>>

}