package app.storytel.candidate.com.data.post.remote

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.post.remote.model.PostAndPhotoModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class PostRemoteDataSource(private val postService: PostService) {

    fun getPosts(): Observable<Resource<List<PostAndPhotoModel>>> {
        return Observable.create { emitter ->

            emitter.onNext(Resource.loading(null))

            postService.getPosts()
                    .zipWith(postService.getPhotos(), PostAndPhotosCombiner())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            {
                                emitter.onNext(Resource.success(it))
                                emitter.onComplete()
                            },
                            {
                                emitter.onNext(Resource.error(null, it))
                                emitter.onComplete()
                            })
        }
    }
}