package app.storytel.candidate.com.ui.detail

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.comment.remote.model.CommentModel

data class CommentsActivityViewState(val commentsResponseResource: Resource<List<CommentModel>>) {

    fun isLoading(): Boolean {
        return commentsResponseResource.isLoading()
    }

    fun hasError(): Boolean {
        return commentsResponseResource.isError()
    }

    fun getCommentItemViewStateList(): List<CommentItemViewState> {
        val commentItemViewStateList = arrayListOf<CommentItemViewState>()
        commentsResponseResource.data?.forEach { commentItemViewStateList.add(CommentItemViewState(it)) }
        return commentItemViewStateList
    }
}