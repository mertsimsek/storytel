package app.storytel.candidate.com.data.comment.remote

import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.comment.remote.model.CommentModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class CommentRemoteDataSource(private val commentService: CommentService) {

    fun getComments(postId: String): Observable<Resource<List<CommentModel>>> {
        return Observable.create { emitter ->
            emitter.onNext(Resource.loading(null))

            commentService.getComments(postId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            {
                                emitter.onNext(Resource.success(it))
                                emitter.onComplete()
                            },
                            {
                                emitter.onNext(Resource.error(null, it))
                                emitter.onComplete()
                            })
        }
    }
}