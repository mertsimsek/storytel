package app.storytel.candidate.com.ui.detail

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.ItemCommentBinding
import app.storytel.candidate.com.databinding.ItemPostBinding
import app.storytel.candidate.com.ui.post.PostItemViewState
import app.storytel.candidate.com.util.extensions.inflateAdapterItem
import java.lang.IllegalStateException

class CommentListAdapter : RecyclerView.Adapter<CommentListAdapter.CommentItemViewHolder>() {

    private val commentItemViewStateList = arrayListOf<CommentItemViewState>()

    var itemClickListener: ((CommentItemViewState) -> Unit)? = null

    fun setCommentItemViewStates(commentItemViewStateList: List<CommentItemViewState>) {
        this.commentItemViewStateList.clear()
        this.commentItemViewStateList.addAll(commentItemViewStateList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentItemViewHolder = CommentItemViewHolder.create(parent, itemClickListener)

    override fun getItemCount(): Int = commentItemViewStateList.size

    override fun onBindViewHolder(holder: CommentItemViewHolder, position: Int) = holder.bind(commentItemViewStateList[position])

    class CommentItemViewHolder(private val binding: ItemCommentBinding,
                                private val itemClickListener: ((CommentItemViewState) -> Unit)?) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.layoutCommentRoot.setOnClickListener {
                itemClickListener?.invoke(binding.itemViewState
                        ?: throw IllegalStateException("ViewState is not set"))
            }
        }

        fun bind(commentItemViewState: CommentItemViewState) {
            binding.itemViewState = commentItemViewState
            binding.executePendingBindings()
        }

        companion object {

            fun create(parent: ViewGroup, itemClickListener: ((CommentItemViewState) -> Unit)?): CommentItemViewHolder {
                val binding: ItemCommentBinding = parent.inflateAdapterItem(R.layout.item_comment)
                return CommentItemViewHolder(binding, itemClickListener)
            }
        }
    }
}