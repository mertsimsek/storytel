package app.storytel.candidate.com.data.post.remote

import app.storytel.candidate.com.data.post.remote.model.PhotoModel
import app.storytel.candidate.com.data.post.remote.model.PostAndPhotoModel
import app.storytel.candidate.com.data.post.remote.model.PostModel
import io.reactivex.functions.BiFunction
import kotlin.random.Random

class PostAndPhotosCombiner : BiFunction<List<PostModel>, List<PhotoModel>, List<PostAndPhotoModel>> {

    override fun apply(posts: List<PostModel>, photos: List<PhotoModel>): List<PostAndPhotoModel> {
        val postAndPhotoModelList = arrayListOf<PostAndPhotoModel>()

        posts.forEach {
            val random = Random.nextInt(0, posts.size)
            postAndPhotoModelList.add(PostAndPhotoModel(it, photos[random]))
        }

        return postAndPhotoModelList
    }
}