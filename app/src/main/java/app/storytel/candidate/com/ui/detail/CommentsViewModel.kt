package app.storytel.candidate.com.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.storytel.candidate.com.data.Resource
import app.storytel.candidate.com.data.comment.remote.CommentRemoteDataSource
import app.storytel.candidate.com.data.comment.remote.CommentService
import app.storytel.candidate.com.data.comment.remote.model.CommentModel
import app.storytel.candidate.com.repository.comment.CommentRepository
import app.storytel.candidate.com.util.extensions.disposeIfNeed
import app.storytel.candidate.com.util.extensions.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CommentsViewModel(private val commentRepository: CommentRepository) : ViewModel() {

    private val commentsActivityViewStateLiveData = MutableLiveData<CommentsActivityViewState>()

    private val compositeDisposable = CompositeDisposable()

    private var postId: String? = null

    fun initialize(postId: String?) {
        this.postId = postId

        if (postId.isNullOrEmpty()) {
            val postIdError = Throwable("Post Id is not valid!")
            val postIdErrorResource = Resource.error(listOf<CommentModel>(), postIdError)
            commentsActivityViewStateLiveData.value = CommentsActivityViewState(postIdErrorResource)
            return
        }

        compositeDisposable += commentRepository.getComments(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { commentsActivityViewStateLiveData.value = CommentsActivityViewState(it) }
    }

    fun getCommentsActivityViewState(): LiveData<CommentsActivityViewState> = commentsActivityViewStateLiveData

    fun refresh() {
        initialize(this.postId)
    }

    override fun onCleared() {
        compositeDisposable.disposeIfNeed()
        super.onCleared()
    }

}